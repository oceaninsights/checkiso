//
//  main.swift
//  checkiso
//
//  Created by Martin Kautz on 29.01.18.
//  Copyright © 2018 Martin Kautz. All rights reserved.
//

import Foundation

if CommandLine.argc != 3 {
    print("Please provide exactly two arguments! First one must be the isofile as newline separated list, second one must be the CSS file to check.")
} else {

    let isoFilePath = CommandLine.arguments[1]
    let cssFilePath = CommandLine.arguments[2]
    
    let fileManager = FileManager.default
    
    let cssFileUrl = URL(fileURLWithPath:cssFilePath)
    
    var cssFileContent = ""
    
    if fileManager.fileExists(atPath:cssFilePath){
        do {
            cssFileContent = try String(contentsOf: cssFileUrl, encoding: .utf8)
        }
        catch { print ("error reading css file into a string!") }
    }
    else {
        print ("CSS file does not exists!")
    }
    
    
    
    let isoFileUrl = URL(fileURLWithPath:isoFilePath)
    if fileManager.fileExists(atPath:isoFilePath) {
        do {
            let isoFileContent = try String(contentsOf: isoFileUrl, encoding: .utf8)
            let isoList = isoFileContent.components(separatedBy: .newlines)
            
            for country in isoList {
                let classString = ".flag.flag-\(country.lowercased())"
                //print("Checking \(classString)")
                if cssFileContent.range(of:classString) != nil {
                    //print("\(classString) exists!")
                } else {
                    print("Huh! \(country) not referred in CSS! ")
                }
            }
            
        }
        catch { print ("error reading iso file into a string!") }
    }
    else {
        print ("ISO file does not exists!")
    }
    
}
