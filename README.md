# CheckIso


Command line tool to to check if some classes exists in a CSS file.
Class names to check for are assembled from a country list with iso codes.

This is to check if the [flag sprites generator](https://www.flag-sprites.com/) covers the all countries we're using (e.g. in our vessel database).


